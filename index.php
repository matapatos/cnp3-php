<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/8/17
 * Time: 11:38 PM
 */

require_once ('vendor/autoload.php');
$f3 = Base::instance();

$f3->config('configs/setup.cfg');
$f3->config('configs/routes.cfg');

//Set up DB connection
if(!$f3->exists('DB')) {
    $f3->set('DB', new DB\SQL(
        $f3->get('db_connection'),
        $f3->get('db_user'),
        $f3->get('db_password')
    ));
}


$f3->run();