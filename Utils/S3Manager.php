<?php
use Aws\S3\S3Client;

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/20/17
 * Time: 3:20 PM
 */
class S3Manager
{
    private $bucket;
    private $client;

    public function __construct(Base $f3)
    {
        $this->bucket = $f3->get('aws_bucket');
        $this->client = new S3Client(array(
            'region' => 'eu-west-1',
            'version' => 'latest',
            'credentials' => array(
                'key'    => $f3->get('aws_access_key_id'),
                'secret' => $f3->get('aws_secret_access_key'),
            )
        ));
    }


    public function uploadProfilePicture($filepath, $userid) {
        // Upload a file.
        $remote_filepath = "profile-pictures/" . $userid . "/photo.png";
        return $this->uploadImage($filepath, $remote_filepath);
    }

    private function uploadImage($host_filepath, $r_filepath) {
        $result = $this->client->putObject(array(
            'Bucket'       => $this->bucket,
            'Key'          => $r_filepath,
            'SourceFile'   => $host_filepath,
            'ContentType'  => 'image/xyz',
            'ACL'          => 'public-read',
            'StorageClass' => 'REDUCED_REDUNDANCY'
        ));

        return $this->client->getObjectUrl($this->bucket, $r_filepath);
    }
}