<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/9/17
 * Time: 11:13 AM
 */
class HttpResponse
{
    public static function error($message, $error_code = 400) {
        if(!is_array($message))
            $message = array('error' => $message, 'error_code' => $error_code);

        header('Content-Type: application/json');
        exit(json_encode($message));
    }

    public static function success($message) {
        if(!is_array($message))
            $message = array('message' => $message);

        header('Content-Type: application/json');
        exit(json_encode($message));
    }

    public static function errorMissingParameter($paramName) {
        HttpResponse::error('Missing required parameter \'' . $paramName . '\'', 400);
    }
}