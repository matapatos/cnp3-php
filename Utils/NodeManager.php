<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/31/17
 * Time: 3:25 PM
 */
class NodeManager
{
    const NODE_URI_NOT_DEFINED = "The URI of NodeJS was not defined",
          NOTIFYING_NODE_GENERAL_ERROR = "Notifying NodeJS general error",
          NODE_SECRET_NOT_DEFINING = "No secret is defined for NodeJS",
          ERROR_COMMUNICATING_WITH_NODE = "An error occoured when trying to notify NodeJS server";

    public static final function notifyRemovedFriend(Base $f3, $userid1, $userid2) {
        $node_url = self::getValidNodeURL($f3, '/friends/remove');
        self::notifyNode($f3, $node_url, array('userid1' => $userid1, 'userid2' => $userid2));
    }

    public static final function notifyAddedFriend(Base $f3, $userid1, $userid2) {
        $node_url = self::getValidNodeURL($f3, '/friends/add');
        self::notifyNode($f3, $node_url, array('userid1' => $userid1, 'userid2' => $userid2));
    }

    public static final function updateAllFriends(Base $f3, $json) {
        $node_url = self::getValidNodeURL($f3, '/friends/update');
        self::notifyNode($f3, $node_url, null, $json, 'POST');
    }

    private static final function notifyNode(Base $f3, $url, $params, $body = null, $method = 'PUT') {
        $nodejs_secret = $f3->get('nodejs_secret');
        if(empty($nodejs_secret))
            HttpResponse::error(self::NODE_SECRET_NOT_DEFINING);

        else {
            try {
                $options = array(
                    'method'  => $method,
                    'header' => array('Authorization: ' . $nodejs_secret)
                );
                if($params) {
                    $url .= '?' . http_build_query($params);
                }
                if($body) {
                    $options['content'] = json_encode($body);
                }

                $result = \Web::instance()->request($url, $options);
                if(!$result)
                    HttpResponse::error(self::ERROR_COMMUNICATING_WITH_NODE, 500);
                else if(!empty($result['error']))
                    HttpResponse::error($result['error'], 500);
                else {
                    $json_body = json_decode($result['body']);
                    if(!$json_body || $json_body->error)
                        HttpResponse::error($json_body->error, 500);
                }
            }
            catch(Exception $ex) {
                HttpResponse::error(self::NOTIFYING_NODE_GENERAL_ERROR, 500);
            }
        }
    }

    // Auxiliary methods

    private static final function getValidNodeURL(Base $f3, $request) {
        $node_uri = self::getNodeURL($f3);
        return $node_uri . $request;
    }

    private static final function getNodeURL(Base $f3) {
        $node_uri = $f3->get('nodejs_rest_uri');
        if(empty($node_uri)) {
            HttpResponse::error(self::NODE_URI_NOT_DEFINED, 500);
        }
        return $node_uri;
    }
}