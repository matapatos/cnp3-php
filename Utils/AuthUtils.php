<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/11/17
 * Time: 10:34 AM
 */
class AuthUtils
{
    const VALID_JWT = 0,
          EXPIRED_JWT = 1,
          INVALID_JWT = 2;

    const DAY_MILLISECONDS = 86400;
    const BEARER_JWT = 'Bearer ';

    const USER_ID_JWT_FIELD = 'sub',
          EXPIRED_TIME_FIELD = 'exp',
          CREATION_TIME_FIELD = 'iat',
          HOSTNAME_FIELD = 'iss';

    public static function getVerifiedUserIdOfToken(Base $f3) {
        AuthUtils::verifyUserAuth($f3); //Verify and return error if user not defined

        $userid = AuthUtils::getUserIdOfToken($f3);
        if($userid == false) {
            HttpResponse::error(UserController::INVALID_TOKEN_DATA, 400);
        }
        return $userid;
    }

    public static function verifyUserAuth(Base $f3) {
        $status = AuthUtils::getAuthStatus($f3);
        if ($status == AuthUtils::INVALID_JWT)
            HttpResponse::error(UserController::USER_NOT_AUTHENTICATED_ERROR, 403);
        else if ($status == AuthUtils::EXPIRED_JWT)
            HttpResponse::errorExpiredToken(UserController::USER_TOKEN_EXPIRED_ERROR);

        else if($status != AuthUtils::VALID_JWT) //To make sure that doesn't exists more errors
            HttpResponse::error(UserController::USER_AUTHENTICATION_ERROR, 500);
    }

    public static function getAuthStatus(Base $f3) {
        $token = self::getToken($f3);
        return self::getTokenStatus($f3, $token);
    }

    public static function getUserIdOfToken(Base $f3) {
        $token = self::getToken($f3);
        $token_data = self::getTokenData($f3, $token);
        $userid = $token_data->sub;
        if(is_int($userid) && $userid > 0)
            return $userid;

        else return false;
    }

    public static function getGeneratedJWTToken($userid) {
        try {
            $f3 = Base::instance();
            $secretKey = base64_decode(self::getSecret($f3));
            $now = time();
            $data = array(self::USER_ID_JWT_FIELD => $userid, self::EXPIRED_TIME_FIELD => $now + self::DAY_MILLISECONDS,
                          self::CREATION_TIME_FIELD => $now, self::HOSTNAME_FIELD => gethostname());
            $jwt = \Firebase\JWT\JWT::encode(
                $data,
                $secretKey,
                'HS256'
            );
            return self::BEARER_JWT . $jwt;
        }
        catch(Exception $ex) {
            HttpResponse::error(UserController::GENERATING_TOKEN_ERROR, 500);
        }
    }

    public static function getFacebookUserDataByToken(Base $f3, $fb_access_token) {
        $fb = new \Facebook\Facebook([
            'http_client_handler' => 'stream', //Quick fix for being able to use Facebook PHP SDK API and AWS S3 API
            'app_id' => $f3->get('fb_app_id'),
            'app_secret' => $f3->get('fb_app_secret'),
            'default_graph_version' => 'v2.9'
        ]);

        $fb->setDefaultAccessToken($fb_access_token);

        try{
            $userInfor = $fb->get('/me?fields=name,email,picture', $fb_access_token);
            $body_array = $userInfor->getDecodedBody();
            if(!is_array($body_array['picture']) || !key_exists('id', $body_array) || !key_exists('name', $body_array) || !key_exists('email', $body_array) || !key_exists('picture', $body_array)) {
                HttpResponse::error(UserController::INVALID_FACEBOOK_REQUEST, 400);
            }

            if(key_exists('data', $body_array['picture']) && key_exists('url', $body_array['picture']['data'])) {
                $body_array['picture'] = $body_array['picture']['data']['url'];
            }
            else $body_array['picture'] = null;

            return $body_array;

        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            HttpResponse::error(UserController::USER_NOT_AUTHENTICATED_ERROR, 400);
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            HttpResponse::error(UserController::FACEBOOK_LOGIN_GENERAL_ERROR, 500);
        }
        return false;
    }

    private static function getToken(Base $f3) {
        $token = $f3->get('TOKEN');
        if(empty($token)) {
            foreach($f3->get('HEADERS') as $key => $value) {
                echo $key . " : " . $value . "\n\r";
                if(strtolower($key) == 'authorization') {
                    $token = str_replace(self::BEARER_JWT, '', $value);
                    $f3->set('TOKEN', $token);
                    break;
                }
            }
        }
        return $token;
    }

    private static function getTokenData(Base $f3, $token) {
        $token_data = $f3->get('TOKEN_DATA');
        if(empty($token_data)) {
            if(self::getTokenStatus($f3, $token) == self::VALID_JWT)
                $token_data = $f3->get('TOKEN_DATA');

            else $token_data = null;
        }
        return $token_data;
    }

    private static function getTokenStatus(Base $f3, $token) {
        try {
            $secretKey = base64_decode(self::getSecret($f3));
            $data = \Firebase\JWT\JWT::decode(
                $token,
                $secretKey,
                array('HS256'));
            $f3->set('TOKEN_DATA', $data);
            return self::VALID_JWT;
        }
        catch(\Firebase\JWT\ExpiredException $ex) {
            return self::EXPIRED_JWT;
        }
        catch (Exception $ex) {
            return self::INVALID_JWT;
        }
    }

    private static function getSecret(Base $f3) {
        $jwt_secret = $f3->get('jwt_secret');
        if(empty($jwt_secret))
            HttpResponse::error(self::NO_JWT_SECRET_DEFINED, 500);
        return $jwt_secret;
    }
}