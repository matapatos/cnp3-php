<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/20/17
 * Time: 4:57 PM
 */
class FileUploadManager
{
    const UPLOADING_IMAGE_ERROR = 'An error occoured when trying to upload an image. Try again';

    public static function receiveImage() {
        $overwrite = false; // set to true, to overwrite an existing file; Default: false
        $slug = true; // rename file to filesystem-friendly version
        $web = \Web::instance();
        $files = $web->receive(function($file,$formFieldName){

            // maybe you want to check the file size
            if($file['size'] > (2 * 1024 * 1024)) // if bigger than 2 MB
                return false; // this file is not valid, return false will skip moving it

            //Check file extension
            $filename_parts = pathinfo($file['name']);
            $extension = $filename_parts['extension'];
            if(empty($extension))
                return false;

            $extension = strtolower($extension);
            if(!$extension == 'jpg' && !$extension == 'png')
                return false;

            // everything went fine, hurray!
            return true; // allows the file to be moved from php tmp dir to your defined upload dir
        },
            $overwrite,
            $slug
        );

        foreach ($files as $filepath => $hasUploaded) {
            if($hasUploaded) {
                return $filepath;
            }
            else {
                HttpResponse::error(self::UPLOADING_IMAGE_ERROR, 500);
            }
            break;
        }

        return false;
    }
}