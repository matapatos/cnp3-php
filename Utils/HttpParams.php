<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/9/17
 * Time: 11:22 AM
 */
class HttpParams
{
    const REQUIRED_UNSIGNED_INT = '\'pageid\' should have a value bigger than \'0\'';

    public static function getRequiredUInt($f3, $name) {
        $param = self::getRequiredInt($f3, $name);
        if($param > 0) {
            return $param;
        }
        HttpResponse::error(self::REQUIRED_UNSIGNED_INT, 400);
    }

    public static function getRequiredInt($f3, $name) {
        $param = self::getRequiredParam($f3, $name);
        return intval($param);
    }

    public static function getRequiredParam($f3, $name) {
        $param = self::getOptionalParam($f3, $name);
        if($param != null)
            return $param;

        HttpResponse::errorMissingParameter($name);
    }

    public static function getOptionalBool($f3, $name) {
        $param = self::getOptionalParam($f3, $name);
        if(intval($param) > 0)
            return true;

        return false;
    }

    public static function getOptionalParam($f3, $name) {
        $param = self::GETRequestParams($f3, $name);
        if(empty($param) == false)
            return $param;

        $param = self::POSTRequestParams($f3, $name);
        if(empty($param) == false)
            return $param;

        return null;
    }

    private static function GetRequestParams($f3, $name) {
        return $f3->get('GET.' . $name);
    }

    private static function PostRequestParams($f3, $name) {
        return $f3->get('POST.' . $name);
    }
}