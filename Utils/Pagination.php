<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/23/17
 * Time: 12:16 PM
 */
class Pagination
{
    private $pageid;
    private $userid;
    private $retrieveNumberOfPages;
    private $indexpageid;

    private $friendDAO;
    private $friendshipDAO;
    private $f3;
    private $userDAO;
    private $limit;

    function __construct(Base $f3, UserDAO $userDAO, FriendDAO $friendDAO = null, FriendshipRequestDAO $friendshipDAO = null, $limit)
    {
        $this->f3 = $f3;
        $this->friendDAO = $friendDAO;
        $this->friendshipDAO = $friendshipDAO;
        $this->userDAO = $userDAO;
        $this->limit = $limit;
    }

    function retrieve() {
        if($this->verifyRequest())
            return $this->allOfUser();
        return false;
    }

    function retrieveSearchResults() {
        $query = HttpParams::getRequiredParam($this->f3, 'query');
        if($this->verifyRequest()) {
            return $this->searchUser($query);
        }
        return false;
    }

    private function searchUser($query, $convert = true) {
        $options = $this->getDefaultPaginationOptions();
        $options['order'] = 'name ASC';

        $results = $this->userDAO->searchUserBy($query, $this->userid, $options);
        $results = $this->selectDistinctUsers($results);

        $data = array('results' => $results);
        if($this->retrieveNumberOfPages) {
            $totalPages = $this->getPaginationTotalNumberOfPages($results, $query);
            $data['numPages'] = $totalPages;
        }

        return $data;
    }

    private function verifyRequest() {
        //Get Unsigned Integer
        $this->pageid = HttpParams::getRequiredUInt($this->f3, 'pageid');
        $this->indexpageid = $this->pageid - 1; //The first page starts with 1 but in MySQL the first page starts with 0, that's why we need to reduce by 1 value

        //Get to retrieve number of pages of users
        $this->retrieveNumberOfPages = HttpParams::getOptionalBool($this->f3, 'numPages');

        //Verify user existence
        $this->userid = AuthUtils::getVerifiedUserIdOfToken($this->f3); //Token is already verified in this function

        $user = $this->userDAO->getUserById($this->userid);
        if($user == null) {
            HttpResponse::error(UserController::USER_DONT_EXIST, 400);
        }
        else return true;
    }

    private function allOfUser() {
        //Return friends requests order by creation_date
        $options = $this->getDefaultPaginationOptions();
        $options['order'] = 'creation_date DESC';

        if($this->friendDAO)
            $results = $this->friendDAO->allOfUser($this->userid, $options);
        else $results = $this->friendshipDAO->allOfUser($this->userid, $options);

        $data_results = array();
        if(count($results) > 0) { //Check if it exists friends using the previous offset
            //Retrieve only an array with friends users IDs
            $othersIDs = $this->getOnlyOthersIDs($results);

            $userFriends = $this->userDAO->getUsers($othersIDs);
            $data_results = UserDAO::convertUsersToArray($userFriends);
        }

        $data = array('results' => $data_results);
        if($this->retrieveNumberOfPages) {
            $totalPages = $this->getPaginationTotalNumberOfPages($results);
            $data['numPages'] = $totalPages;
        }

        return $data;
    }

    private function getDefaultPaginationOptions() {
        return array(
            'offset' => $this->indexpageid * $this->limit,
            'limit' => $this->limit
        );
    }

    private function getPaginationTotalNumberOfPages($results, $query = null) {
        $numResults = count($results);
        if($numResults >= $this->limit || ($numResults == 0 && $this->pageid > 1)) { //If it's true there is only a way of knowing the number of pages: by asking the DB. Lowest performance!!
            if($query) $count = $this->userDAO->countSearchUserBy($query);

            else {
                if($this->friendDAO)
                    $count = $this->friendDAO->countAllOfUser($this->userid);

                else $count = $this->friendshipDAO->countAllOfUser($this->userid);
            }

            return ceil($count/$this->limit);
        }
        else { //It means that the current asked page is the last one
            return $this->pageid;
        }
    }

    private function getOnlyOthersIDs($friendResults) {
        $friendsIDs = array();
        foreach($friendResults as $friendship) {
            if($friendship->userid1 == $this->userid)
                array_push($friendsIDs, $friendship->userid2);
            else array_push($friendsIDs, $friendship->userid1);
        }
        return $friendsIDs;
    }

    public function getUserID() {
        return $this->userid;
    }

    private function selectDistinctUsers($users) {
        $new_users = array();
        foreach($users as $u) {
            if(array_key_exists($u['userid'], $new_users)) {
                if($u['is_friend'] != 0) {
                    $new_users[$u['userid']]['is_friend'] = $u['is_friend'];
                }
                if($u['has_friend_request'] != 0){
                    $new_users[$u['userid']]['has_friend_request'] = $u['has_friend_request'];
                }
            }
            else {
                $new_users[$u['userid']] = $u;
            }
        }

        $final_users = array();
        foreach($new_users as $key => $u) {
            array_push($final_users, $u);
        }

        return $final_users;
    }
}