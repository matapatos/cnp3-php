<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 6/4/17
 * Time: 7:56 PM
 */
class General
{
    public static function getOnlyUsersIDs($users) {
        $usersIDs = array();
        foreach($users as $u) {
            array_push($usersIDs, $u->userid);
        }
        return $usersIDs;
    }

    public static function userIDsArrayToString($userIDs) {
        $str = "(";
        if(count($userIDs) > 0) {
            $str .= $userIDs[0];
            for($i = 1; $i < count($userIDs); $i++) {
                $str .= "," . $userIDs[$i];
            }
        }
        $str .= ")";
        return $str;
    }

    public static function getUserFromUsersByID($userid, $users) {
        foreach($users as $u) {
            if($u->userid == $userid)
                return $u;
        }
        return null;
    }


}