"use strict"
class RedisIOManager {
    constructor(RedisClient, AppConfig, io, socket, userid) {
        var self = this;
        self.RedisClient = RedisClient;
        self.io = io;
        self.AppConfig = AppConfig;
        self.socket = socket;
        self.userid = userid;

        //Public methods

        self.onConnectionEstablished = function() {
            var successcb = function(data) {
                var user_redis = self.getUserSocketIdRedisPath();
                var new_data = [];
                new_data.push(socket.id);

                if(data) {
                    data = self.transformDataToArray(data);
                    new_data = new_data.concat(data);
                }

                self.setUserSocketId(new_data);
            };
            self.getUserSocketId(successcb);
        };

        self.onMessage = function(event , message) {
            var message_data = { 'img' : null, 'img_original' : null, 'text' : message, 'userid' : self.userid, 'datetime' : new Date() };
            self.socket.emit(event, message_data); //emit for that specific socket that just send the message

            var successcb = function(data) {
                                if(data) {
                                    var friend_success_cb = function(socketids) {
                                        if(socketids) {
                                            for(var i = 0; i < socketids.length; i++) {
                                                var socketid = socketids[i];
                                                self.socket.broadcast.to(socketid).emit(event, message_data); //Emit to specific Socket IDs
                                            }
                                        }                        
                                    };
                                    for(var i = 0; i < data.length; i++) {
                                        var friend_id = data[i];
                                        self.getUserSocketId(friend_success_cb, friend_id); // Get user socketid and for then being able to emit message
                                    }
                                }
                        };
            self.getFriendsOfUser(successcb);
        };

        self.onDisconnect = function() {
            var successcb = function(data) {
                if(data) {
                    for(var i = 0; i < data.length; i++) {
                        if(data[i] == self.socket.id) {
                            data.splice(i, 1);
                            break;
                        }
                    }
                    self.setUserSocketId(data);
                }
            };
            self.getUserSocketId(successcb);
        };

        //Private methods

        self.getFriendsOfUser = function(successcb) {
            var friends_of_user = self.getFriendsOfUserRedisPath();
            self.RedisClient.get(friends_of_user, function(err, data){
                if(err)
                    console.error('Couldn\'t retrieve the ' + friends_of_user);
                else {
                    if(typeof successcb === 'function')
                        successcb(self.transformDataToArray(data));
                }
            });
        };

        self.getUserSocketId = function(successcb, userid) {
            if(!userid)
                userid = self.userid;

            var socketid_of_user = self.getUserSocketIdRedisPath(userid);
            self.RedisClient.get(socketid_of_user, function(err, data) {
                if(err)
                    console.error('Couldn\'t retrieve the ' + socketid_of_user);
                else {
                    if(typeof successcb === 'function')
                        successcb(self.transformDataToArray(data));
                }
            });
        };

        self.setUserSocketId = function(socketIdData) {
            socketIdData = self.transformDataToString(socketIdData);//Transform data to string

            var socketid_of_user = self.getUserSocketIdRedisPath(); 
            self.RedisClient.set(socketid_of_user, socketIdData, function(err) {
                if(err)
                    console.error("An error occoured when trying to ");
            });
        };

        //Auxiliary methods

        self.transformDataToString = function(data)  {
            return data.toString();
        };

        self.transformDataToArray = function(data) {
            if(data)
                return data.toString().split(',');
            return null;
        };

        self.getUserSocketIdRedisPath = function(userid) {
            if(!userid)
                userid = self.userid;

            return self.AppConfig.RedisUserSocketIdPath + userid;
        };

        self.getFriendsOfUserRedisPath = function() {
            return self.AppConfig.RedisFriendsOfUserPath + self.userid;
        };
    }
}

module.exports = RedisIOManager;