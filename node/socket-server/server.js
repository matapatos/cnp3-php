"use strict";

class SocketServer {
    constructor(AppConfig, MongoDB) {
        var self = this;
        self.AppConfig = AppConfig;
        self.MongoDB = MongoDB;
        self.redisClient = null;

        self.start = function() {
            self.io = self.createSocketServer(self.AppConfig);
            self.io = self.getSecuredJWTSocketServer(self.AppConfig, self.io);
            self.io = self.getServerWithSocketRoutes(self.io);
            self.io = self.setUpRedisStorage(self.AppConfig, self.io);
            self.listen(self.AppConfig, self.io);
        }; 

        self.createSocketServer = function(AppConfig) {
            var express = require('express');
            var app = express();
            app.set('port', AppConfig.socketListenPort);
            var server = require('http').createServer(app);
            var io = require('socket.io')(server);

            return io;        
        };

        self.getSecuredJWTSocketServer = function(AppConfig, io) {
            return require('./security/socket-jwt-auth.js')(io, AppConfig);
        };

        self.getServerWithSocketRoutes = function(io) {
            var RedisIOManager = require('./redis/RedisIOManager.js');
            var messageEvent = 'chat message';
            io.on('connection', function(socket) {
                // now you can access user info through socket.request.user 
                // socket.request.user.logged_in will be set to true 
                socket.emit('success', {
                    message: 'success logged in!'
                });

                var userid = socket.request.user;
                var riom = new RedisIOManager(self.redisClient, self.AppConfig, io, socket, userid);
                riom.onConnectionEstablished(); //Save/Update SocketID of userid

                socket.on(messageEvent, function(message) {
                    riom.onMessage(messageEvent, message);
                    self.MongoDB.addMessage(userid, message, null, null);
                });

                socket.on('disconnect', function() {
                    riom.onDisconnect();
                });                
            });            

            return io;
        };

        self.setUpRedisStorage = function(AppConfig, io) {
            //Create Redis storage for saving socketIDs
            var redis = require('redis');
            self.redisClient = redis.createClient({
                host : AppConfig.RedisHostServer,
                port : AppConfig.RedisPortServer,
                requestsTimeout : AppConfig.RedisRequestsTimeout
            });

            return io;
        };

        self.listen = function(AppConfig, io) {
            io.listen(AppConfig.socketListenPort);
            return io;
        };
    };
};

module.exports = SocketServer;
