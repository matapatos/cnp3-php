module.exports = function(io, AppConfig) {
    var jwtAuth = require('socketio-jwt-auth');

    io.use(jwtAuth.authenticate({
        secret: AppConfig.JWTSecret,    // required, used to verify the token's signature 
        algorithm: AppConfig.authAlgorithm        // optional, default to be HS256 
    }, function(payload, done) {
    // done is a callback, you can use it as follow
        /*
        User.findOne({id: payload.sub}, function(err, user) {
        if (err) {
        // return error 
        return done(err);
        }
        if (!user) {
        // return fail with an error message 
        return done(null, false, 'user not exist');
        }
        // return success with a user info 
        return done(null, user);
    });*/
        return done(null, payload.sub);
    }));  

    return io;    
};
