"use strict";
// Setup basic express server
var AppConfig = require('./configs/app.config.js').AppConfig;
var MongoDB = require('./db/mongo.js');
var SocketServer = require('./socket-server/server.js');
var RestServer = require('./rest-server/server.js');


let mongo = new MongoDB(AppConfig.URIMongoDB);

//Create Socket server
var socketServer = new SocketServer(AppConfig, mongo);
socketServer.start();

//Create Restify server
var rest = new RestServer(AppConfig, mongo);
rest.start();
