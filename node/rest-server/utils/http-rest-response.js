"use strict";
class RestResponse {
}

RestResponse.sendErrorResponse = function(res, next, error_message, error_code) {
    if(!error_code)
        error_code = 400;

    res.send({'error' : error_message, 'error_code' : error_code });
    
    if(typeof next === 'function')
        return next();
};

RestResponse.sendSuccessResponse = function(res, next, message) {
    if(message && (message.constructor === Array || message.constructor === Object))
        res.send(message);
        
    else res.send( {'message' : message } );
    
    if(typeof next === 'function')
        return next();
};

module.exports = RestResponse;