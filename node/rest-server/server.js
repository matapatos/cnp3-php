"use strict";

class RestServer {
    constructor(AppConfig, MongoDB) {
        var self = this;

        self.AppConfig = AppConfig;
        self.MongoDB = MongoDB;
        self.rfm = null;

        //Public Methods
        self.start = function() {
            self.server = self.createRestifyServer();
            self.server = self.getSecuredJWTRestServer(self.server, self.AppConfig);
            self.setupRedisClient(self.AppConfig);

            self.server = self.getServerWithRoutes(self.server);
            self.listen(self.AppConfig, self.server);
        };

        //Private methods

        self.createRestifyServer = function() {
            var restify = require('restify');
            var server = restify.createServer({
                name: AppConfig.restAppName,
                version: AppConfig.restVersion
            });

            server.use(restify.acceptParser(server.acceptable));
            server.use(restify.queryParser());
            server.use(restify.bodyParser());

            server = self.enableCrossDomain(server, restify, AppConfig);

            return server;
        };

        self.enableCrossDomain = function(server, restify, AppConfig) {
            function corsHandler(req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', AppConfig.ApacheHost);
                res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token, Authorization');
                res.setHeader('Access-Control-Allow-Methods', '*');
                res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
                res.setHeader('Access-Control-Max-Age', '1000');
                return next();
            }

            function optionsRoute(req, res, next) {
                res.send(200);
                return next();
            }

            server.use(restify.CORS({
                credentials: true,                 // defaults to false
                methods: ['GET','PUT','DELETE','POST','OPTIONS']
            }));

            /*
            routes and authentication handlers 
            */

            server.opts('/\.*/', corsHandler, optionsRoute);

            return server;
        };

        self.getSecuredJWTRestServer = function(server, AppConfig) {
            return require('./security/rest-jwt-auth.js')(server, AppConfig); //Add Token verification from Http Requests
        };

        self.getServerWithRoutes = function(server) {
            var RestResponse = require('./utils/http-rest-response.js');

            server.post('/friends/update', function (req, res, next) {
                if(!self.isTrustyServer(req))
                    return RestResponse.sendErrorResponse(res, next, "Forbidden", 403);
                else {
                    var data = req.body;
                    console.log(data);
                    if(data) {
                        data = JSON.parse(data);
                        if("users" in data) {
                            var count = 2;
                            var users = data.users;
                            var cb = function(err) {
                                if(err)
                                    console.error(err);
                                else {
                                    count -= 1;
                                    if(count <= 0) {
                                        for(var k in users) {
                                            if(users.hasOwnProperty(k)) {
                                                var user = users[k];
                                                if("userid" in user && "friends" in user) {
                                                    var friends = user.friends;
                                                    self.rfm.addAllFriends(user.userid, friends); //Add new friends to Redis
                                                    self.MongoDB.addAllFriends(user.userid, friends);
                                                }          
                                            }
                                        }
                                        return RestResponse.sendSuccessResponse(res, next, "Updated friends");
                                    }
                                }
                            };
                            //Delete current data
                            //MongoDB
                            self.MongoDB.removeAllFriends(cb);
                            //Redis
                            self.rfm.removeAllFriends(cb);
                            
                        }
                        else {
                            return RestResponse.sendErrorResponse(res, next, "Missing users data on body", 400);
                        }
                    }
                    else {
                        return RestResponse.sendErrorResponse(res, next, "Missing body", 400);
                    }
                }
            });

            server.put('/friends/add', function (req, res, next) {
                if(!req || !("params" in req))
                    return RestResponse.sendErrorResponse(res, next, "Invalid request");
                else if(!self.isTrustyServer(req))
                    return RestResponse.sendErrorResponse(res, next, "Forbidden", 403);
                else if(!("userid1" in req.params))
                    return RestResponse.sendErrorResponse(res, next, "Missing required parameter 'userid1'");
                else if(!("userid2" in req.params))
                    return RestResponse.sendErrorResponse(res, next, "Missing required parameter 'userid2'");
                else {
                    var userid1 = parseInt(req.params.userid1),
                        userid2 =  parseInt(req.params.userid2);
                    
                    if(userid1 <= 0 || userid2 <= 0 || userid1 == userid2)
                        return RestResponse.sendErrorResponse(res, next, "Invalid parameters");
                    else {
                        self.MongoDB.addFriends(userid1, userid2);
                        self.rfm.addFriends(userid1, userid2);
                        return RestResponse.sendSuccessResponse(res, next, "Added friends");
                    }
                }
            });

            server.put('/friends/remove', function (req, res, next) {
                if(!req || !("params" in req))
                    return RestResponse.sendErrorResponse(res, next, "Invalid request");
                else if(!self.isTrustyServer(req))
                    return RestResponse.sendErrorResponse(res, next, "Forbidden", 403);
                else if(!("userid1" in req.params))
                    return RestResponse.sendErrorResponse(res, next, "Missing required parameter 'userid1'");
                else if(!("userid2" in req.params))
                    return RestResponse.sendErrorResponse(res, next, "Missing required parameter 'userid2'");
                else {
                    var userid1 = parseInt(req.params.userid1),
                        userid2 =  parseInt(req.params.userid2);
                    
                    if(userid1 <= 0 || userid2 <= 0 || userid1 == userid2)
                        return RestResponse.sendErrorResponse(res, next, "Invalid parameters");
                    else {
                        self.MongoDB.removeFriends(userid1, userid2);
                        self.rfm.removeFriends(userid1, userid2);
                        return RestResponse.sendSuccessResponse(res, next, "Removed friends");
                    }
                }
            });

            server.get('/messages', function (req, res, next) {
                if(!req || !("params" in req))
                    return RestResponse.sendErrorResponse(res, next, "Invalid request");
                else if(!("page" in req.params))
                    return RestResponse.sendErrorResponse(res, next, "Missing required parameter 'page'");
                else {
                    var page = parseInt(req.params.page);
                    if(page < 1)
                        return RestResponse.sendErrorResponse(res, next, "Invalid page number");
                    else {
                        var userid = req.user.sub; //Token userid
                        var callback = function(err, doc) {
                            if(err) {
                                return RestResponse.sendErrorResponse(res, next, "An error occoured when trying to retrieve the latest messages");
                            }
                            else {
                                return RestResponse.sendSuccessResponse(res, next, { message : "Retrieved messages with success", data : doc });
                            }
                        };
                        self.MongoDB.getLatestFriendsAndUserMessagesByPage(userid, page, self.AppConfig.numMessagesPerPage, callback);
                    }
                }
            });

            server.get('/messages/user', function(req, res, next) {
                if(!req || !("params" in req))
                    return RestResponse.sendErrorResponse(res, next, "Invalid request");
                else if(!("page" in req.params))
                    return RestResponse.sendErrorResponse(res, next, "Missing required parameter 'page'");
                else {
                    var page = parseInt(req.params.page);
                    var useridForMessages = req.user.sub;
                    if("userid" in req.params) {
                        var uuid = parseInt(req.params.userid);
                        if(uuid > 0)
                            useridForMessages = uuid;
                    }
                    if(page < 1)
                        return RestResponse.sendErrorResponse(res, next, "Invalid page number");
                    else if(useridForMessages < 1)
                        return RestResponse.sendErrorResponse(res, next, "Invalid userid");
                    else {
                        var userid = req.user.sub; //Token userid
                        var callback = function(err, doc) {
                            if(err) {
                                return RestResponse.sendErrorResponse(res, next, err);
                            }
                            else {
                                return RestResponse.sendSuccessResponse(res, next, { message : "Retrieved messages with success", data : doc });
                            }
                        };
                        self.MongoDB.getLatestUserMessagesByPage(userid, useridForMessages, page, self.AppConfig.numMessagesPerPage, callback);
                    }
                }
            });

            server.get('/messages/search', function (req, res, next) {
                res.send(req.params);
                return next();
            });

            return server;
        };

        self.setupRedisClient = function(AppConfig) {
            var RedisFriendsManager = require('./redis/RedisFriendsManager.js');
            self.rfm = new RedisFriendsManager(self.AppConfig);
        };

        self.listen = function(AppConfig, server) {
            server.listen(AppConfig.restListenPort, function () {
                console.log('%s listening at %s', server.name, server.url);
            });
        };

        self.isTrustyServer = function(req) {
            if("headers" in req) {
                if("authorization" in req.headers) {
                    var token = req.headers.authorization;
                    if(token == self.AppConfig.trustyServerSecret)
                        return true;
                }
            }
            return false;
        };        
    }
}

module.exports = RestServer;