"use strict";
module.exports = function(server, AppConfig) {
    var rest_jwt = require('restify-jwt');

    server.use(rest_jwt({
        secret: AppConfig.JWTSecret,
        credentialsRequired: true,
        getToken: function fromHeaderOrQuerystring (req) {
            if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
                return req.headers.authorization.split(' ')[1];
            } else if (req.query && req.query.token) {
                return req.query.token;
            }
            return null;
        }
    }).unless({ path : AppConfig.trustyServerPaths}));

    return server;
};