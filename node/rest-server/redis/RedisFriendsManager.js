"use strict";

class RedisFriendsManager {
    constructor(AppConfig, RedisClient) {
        var self = this;
        self.RedisClient = RedisClient;
        self.AppConfig = AppConfig;

        self.createClient = function() {
            var redis = require('redis');
            self.RedisClient = redis.createClient({
                host : AppConfig.RedisHostServer,
                port : AppConfig.RedisPortServer,
                requestsTimeout : AppConfig.RedisRequestsTimeout
            });
        };

        self.removeFriends = function(userid1, userid2) {
            self.removeFriend(userid1, userid2);
            self.removeFriend(userid2, userid1);
        };

        self.removeFriend = function(userid, friendid) {
            var redis_user_path = self.getFriendsOfUserRedisPath(userid);
            self.RedisClient.get(redis_user_path, function(err, data) {
                if(err)
                    console.error(err);
                else {
                    data = self.transformDataToArray(data);
                    if(data) {
                        for(var i = 0; i < data.length; i++) {
                            if(data[i] == friendid) {
                                data.splice(i, 1);
                                self.RedisClient.set(redis_user_path, self.transformDataToString(data), function(err) {
                                    if(err)
                                        console.error(err);
                                });
                                break;
                            }
                        }
                    }
                }
            });
        };

        self.addFriends = function(userid1, userid2) {
            self.addFriend(userid2, userid1);
            self.addFriend(userid1, userid2);
        };

        self.addFriend = function(userid, friendid) {
            var redis_user_path = self.getFriendsOfUserRedisPath(userid);
            self.RedisClient.get(redis_user_path, function(err, data) {
                if(err)
                    console.error(err);
                else {
                    data = self.transformDataToArray(data);
                    var friends = [];
                    friends.push(friendid);
                    if(data) {
                        friends = friends.concat(data);
                    }

                    console.log(friends);
                    self.RedisClient.set(redis_user_path, self.transformDataToString(friends), function(err) {
                        if(err)
                            console.error(err);
                    });
                }
            });
        };

        self.removeAllFriends = function(cb) {
            self.RedisClient.flushdb(function(err) { //Erase all current connection data from Redis
                if(err)
                    console.error(err);
                if(typeof cb === 'function')
                    cb();
            });
        };

        self.addAllFriends = function(userid, friends) {
            var redis_user_path = self.getFriendsOfUserRedisPath(userid);
            self.RedisClient.set(redis_user_path, self.transformDataToString(friends), function(err) {
                if(err)
                    console.error(err);
            });
        };

        self.close = function() {
            if(self.RedisClient)
                self.RedisClient.quit();
        };

        self.transformDataToArray = function(data) {
            if(data)
                return data.toString().split(',');
            return null;
        };

        self.transformDataToString = function(data)  {
            return data.toString();
        };

        self.getFriendsOfUserRedisPath = function(userid) {
            return self.AppConfig.RedisFriendsOfUserPath + userid;
        };

        //initialize RedisClient if not yet
        if(!self.RedisClient)
            self.createClient();
    }
}

module.exports = RedisFriendsManager;