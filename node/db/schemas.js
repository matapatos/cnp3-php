"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require('mongoose');

var Messages = mongoose.Schema({
    _id : Number,
    userid : { type : mongoose.Schema.Types.Number, ref : "Friends", unique : false, required : true },
    datetime : {type : Date, default : Date.now },
    text : String,
    img : String,
    img_original : String
});

var Friends = mongoose.Schema({
    _id : Number,
    friends : []
});

class MongoSchemas {}
MongoSchemas.Messages = Messages;
MongoSchemas.Friends = Friends;

exports.MongoSchemas = MongoSchemas;