"use strict";
var Schemas = require('./schemas.js').MongoSchemas;
const mongoose = require('mongoose');
const auto_increment = require('mongoose-auto-increment');
const mongoose_paginate = require('mongoose-paginate');

class MongoDB {
    constructor(uri) {
        var self = this;
        mongoose.Promise = require('bluebird');
        self.db = mongoose.createConnection(uri);

        //Add auto increment for Messages model
        auto_increment.initialize(self.db);
        Schemas.Messages.plugin(auto_increment.plugin, 'Messages');
        Schemas.Messages.plugin(mongoose_paginate, 'Messages');

        //Initialize models
        self.Friends = self.db.model('Friends', Schemas.Friends);
        self.Messages = self.db.model('Messages', Schemas.Messages);

        self.addFriends = function(userid1, userid2) {
            self.Friends.findById(userid1, function(err, doc) {
                if(err) {
                    console.error(err);
                }
                else {
                    if(doc) {
                        if(userid2 in doc.friends) {
                            console.warn('Trying to add friend that already exists');
                        }
                        else {
                            doc.friends.push(userid2);
                            doc.save();

                        }
                    }
                    else {
                        self.insertUserWithFriend(userid1, userid2);
                    }
                }
            });

            self.Friends.findById(userid2, function(err, doc) {
                if(err) {
                    console.error(err);
                }
                else {
                    if(doc) {
                        if(userid1 in doc.friends) {
                            console.warn('Trying to add friend that already exists');
                        }
                        else {
                            doc.friends.push(userid1);
                            doc.save();
                        }
                    }
                    else {
                        self.insertUserWithFriend(userid2, userid1);
                    }
                }
            });
        };

        self.addAllFriends = function(userid, friends) {
            var friend = new self.Friends({
                _id : userid,
                friends : friends
            });
            friend.save(function(err) {
                if(err) {
                    console.error(err);
                }
            });
        };

        self.removeFriends = function(userid1, userid2) {
            self.Friends.findById(userid1, function(err, doc) {
                if(err) {
                    console.error(err);
                }
                else {
                    if(!doc) {
                        console.warn("Trying to remove friend of unexistent user");
                    }
                    else {
                        var index = doc.friends.indexOf(userid2);
                        if(index < 0) {
                            console.warn("Trying to remove an unexistent friend");
                        }
                        else {
                            doc.friends.splice(index, 1);
                            doc.save();
                        }
                    }
                }
            });

            self.Friends.findById(userid2, function(err, doc) {
                if(err) {
                    console.error(err);
                }
                else {
                    if(!doc) {
                        console.warn("Trying to remove friend of unexistent user");
                    }
                    else {
                        var index = doc.friends.indexOf(userid1);
                        if(index < 0) {
                            console.warn("Trying to remove an unexistent friend");
                            console.warn("userid: " + userid2 + " friendid: " + userid1);
                        }
                        else {
                            doc.friends.splice(index, 1);
                            doc.save();
                        }
                    }
                }
            });
        };

        self.insertUserWithFriend = function(userid_to_be_added, userid_of_friend) {
            var friends = [];
            friends.push(userid_of_friend);

            var friend = new self.Friends({
                _id : userid_to_be_added,
                friends : friends
            });
            friend.save(function(err) {
                if(err) {
                    console.error(err);
                }
            });
        };

        self.getLatestFriendsAndUserMessagesByPage = function(userid, pageid, limitOfPages, callback) {
            self.Friends.findById(userid, function(err, doc){
                if(err) {
                    console.error(err);
                    if(typeof callback === 'function')
                        callback(err, null); //Callback will handle error
                }
                else {
                    var users_ids = [];
                    users_ids.push(userid);
                    if(doc)
                        users_ids = users_ids.concat(doc.friends); //Append friends IDs

                    //Search by userid and friends
                    self.Messages.paginate(
                        { userid : { '$in' : users_ids } }, 
                        { page : pageid, limit : limitOfPages, sort : { datetime : 'desc'} }, 
                        function(err, doc) {
                            if(err)                
                                console.error(err);

                            if(typeof callback === 'function')
                                callback(err, doc); //Callback will handle error or success also

                            else console.warn('No callback assigned for \'MongoDB.getLatestFriendsAndUserMessagesByPage\'');
                        });                       
                }
            });
        };

        self.getLatestUserMessagesByPage = function(userid, useridForMessages, pageid, limitOfPages, callback) {
            self.Friends.findById(userid, function(err, doc){
                if(err) {
                    console.error(err);
                    if(typeof callback === 'function')
                        callback("An error occoured when trying to retrieve the User messages", null); //Callback will handle error
                }
                else {
                    if(!doc && userid != useridForMessages) {
                        if(typeof callback === 'function')
                            callback("Friendship doesn't exist.", null);
                    }
                    else if(userid != useridForMessages && doc.friends.indexOf(useridForMessages) < 0) {
                        if(typeof callback === 'function')
                            callback("Friendship doesn't exist.", null);
                    }
                    else {
                        //Search by userid and friends
                        self.Messages.paginate(
                            { userid : useridForMessages }, 
                            { page : pageid, limit : limitOfPages, sort : { datetime : 'desc'} }, 
                            function(err, doc) {
                                if(err)                
                                    console.error(err);

                                if(typeof callback === 'function')
                                    callback(err, doc); //Callback will handle error or success also

                                else console.warn('No callback assigned for \'MongoDB.getLatestFriendsAndUserMessagesByPage\'');
                            });  
                    }                     
                }
            });
        };

        self.addMessage = function(userid, text, image_url, image_orig_url) {
            var m = new self.Messages(
                {
                    userid : userid,
                    text : text,
                    img : image_url,
                    img_original : image_orig_url
                });
            m.save(function(err) {
                if(err)
                    console.error(err);
            });
        };

        self.removeAllFriends = function(cb) {
            self.Friends.find({}).remove().exec(function(err){
                if(err)
                    console.error(err);
                if(typeof cb === 'function')
                    cb(err);
            });
        };
    }
}

module.exports = MongoDB;
