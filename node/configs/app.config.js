"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AppConfig {
}
//Apache Server
AppConfig.ApacheHost = "**REMOVED**";
//JWT Tokens
AppConfig.JWTSecret = '**REMOVED**';
AppConfig.authAlgorithm = 'HS256';
//Mongo DB
AppConfig.URIMongoDB = '**REMOVED**';
//Sockets server
AppConfig.socketListenPort = 9000;
AppConfig.SocketNamespace = "cnp3";
//Redis server
AppConfig.RedisHostServer = "**REMOVED**";
AppConfig.RedisPortServer = 6379;
AppConfig.RedisRequestsTimeout = 1500;
AppConfig.RedisUserSocketIdPath = 'socket-user-';
AppConfig.RedisFriendsOfUserPath = 'friends-user-';
//Rest server
AppConfig.restListenPort = 8080;
AppConfig.restAppName = 'CNP3 Rest';
AppConfig.restVersion = 'v1.0.0';
AppConfig.numMessagesPerPage = 20;
//Apache server
AppConfig.trustyServerSecret = '**REMOVED**';
AppConfig.trustyServerPaths = ['/friends/remove', '/friends/add', '/friends/update'];

exports.AppConfig = AppConfig;
