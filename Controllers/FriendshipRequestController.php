<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/23/17
 * Time: 12:13 PM
 */
class FriendshipRequestController extends MainController
{
    const LIMIT_OF_FRIENDSHIP_REQUEST_RETRIEVING = 20;

    const ALREADY_EXISTS_FRIENDSHIP_ERROR = "It already exists a Friendship request between both users",
          ERROR_TRYING_TO_MANIPULATE_FRIENDSHIP_REQUEST_WITH_SAME_USER = "Trying to manipulate a Friendship request with the same user",
          FRIENDSHIP_REQUEST_DOESNT_EXISTS_ERROR = "Friendship request doesnt exist";

    const SENT_FRIENDSHIP_REQUEST = "A Friendship request as been sent",
          FRIENDSHIP_REQUEST_REMOVED_WITH_SUCCESS = "Friendship request removed with success";

    function getFriendshipsRequests() {
        //Base $f3, UserDAO $userDAO, DB\SQL\Mapper $requestDAO, $limit
        $pagination = new Pagination($this->f3, new UserDAO($this->db), null, new FriendshipRequestDAO($this->db), self::LIMIT_OF_FRIENDSHIP_REQUEST_RETRIEVING);
        $data = $pagination->retrieve();
        HttpResponse::success($data);
    }

    function askFriendship() {
        $data = $this->getVerifiedRequestData();
        $user = $data['user'];
        $user_result = $data['user_result'];

        $friendshipDAO = new FriendshipRequestDAO($this->db);
        if($friendshipDAO->existsFriendship($user->userid, $user_result->userid)) {
            HttpResponse::error(self::ALREADY_EXISTS_FRIENDSHIP_ERROR, 400);
        }
        else {
            $friendDAO = new FriendDAO($this->db);
            if($friendDAO->areFriends($user->userid, $user_result->userid)) {
                HttpResponse::error(FriendController::ALREADY_FRIENDS_ERROR, 400);
            }
            else {
                $friendshipDAO->addFriendship($user->userid, $user_result->userid);
                HttpResponse::success(self::SENT_FRIENDSHIP_REQUEST);
            }
        }
    }

    function removeFriendship() {
        $data = $this->getVerifiedRequestData();
        $user = $data['user'];
        $user_result = $data['user_result'];

        $friendshipDAO = new FriendshipRequestDAO($this->db);
        if(!$friendshipDAO->existsFriendship($user->userid, $user_result->userid)) {
            HttpResponse::error(self::FRIENDSHIP_REQUEST_DOESNT_EXISTS_ERROR, 400);
        }
        else {
            $friendshipDAO->removeFriendship($user->userid, $user_result->userid);
            HttpResponse::success(self::FRIENDSHIP_REQUEST_REMOVED_WITH_SUCCESS);
        }
    }

    private function getVerifiedRequestData()
    {
        //Get Unsigned Integer
        $username = HttpParams::getRequiredParam($this->f3, 'username');

        //Verify user existence
        $userid = AuthUtils::getVerifiedUserIdOfToken($this->f3); //Token is already verified in this function

        $userDAO = new UserDAO($this->db);
        $user = $userDAO->getUserById($userid);
        if($user == null) {
            HttpResponse::error(UserController::USER_DONT_EXIST, 406);
        }
        else {
            $user_result = $userDAO->getUserByUsername($username);
            if($user_result == null) {
                HttpResponse::error(UserController::USER_DONT_EXIST, 400);
            }
            else {
                if($user->userid == $user_result->userid) {
                    HttpResponse::error(self::ERROR_TRYING_TO_MANIPULATE_FRIENDSHIP_REQUEST_WITH_SAME_USER);
                }
                else return array('user' => $user, 'user_result' => $user_result);
            }
        }
    }

}