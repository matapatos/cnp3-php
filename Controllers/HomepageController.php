<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/8/17
 * Time: 11:46 PM
 */
class HomepageController
{
    function index() {
        $view = \View::instance();
        echo $view->render('index.html');
    }
}