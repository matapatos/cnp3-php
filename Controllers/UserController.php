<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/9/17
 * Time: 12:27 AM
 */
class UserController extends MainController
{
    const LIMIT_OF_USERS_SEARCH_RESULT = 20;

    //Error messages
    const USER_ALREADY_EXISTS_ERROR = 'User already exists',
          USER_REGISTRATION_PROCESS_ENDS_WITH_ERROR = 'User registration ended with an error',
          USER_DONT_EXIST = 'Bad username or password',
          GENERATING_TOKEN_ERROR = 'An error occours during the generation of your token',
          USER_NOT_AUTHENTICATED_ERROR = 'User must be logged in first for doing this request',
          USER_TOKEN_EXPIRED_ERROR = 'User must log in again to be able to do this request',
          INVALID_TOKEN_DATA = 'The token information contained is invalid. Try to logout and login again.',
          BAD_USER_PASSWORD = 'Bad current user password',
          USER_AUTHENTICATION_ERROR = 'Authentication failed from an error from the server. Please contact the administrator',
          FACEBOOK_LOGIN_GENERAL_ERROR = 'An error occoured when trying to logged with Facebook account. Try again.',
          INVALID_FACEBOOK_REQUEST = 'Invalid Facebook request. Please contact the administrator',
          GENERAL_FACEBOOK_REGISTRATION_ERROR = 'A general error occoured when trying to register via Facebook. Please contact administrator',
          GENERAL_FACEBOOK_LOGIN_ERROR = 'A general error occoured when trying to login via Facebook. Please contact administrator';

    //Success messages
    const USER_PASSWORD_CHANGED_WITH_SUCCESSS = 'User password changed with success',
          USER_REGISTERED_WITH_SUCCESS = 'User registered with success',
          LOGIN_SUCCEED = 'Login succeed',
          PROFILE_UPDATED_WITH_SUCCESS = 'Profile updated with success',
          SEARCH_RESULTS_WITH_SUCCESS = 'Search results on attached';

    function register_index() {
        $view = \View::instance();
        echo $view->render('register.html');
    }

    function register() {
        $username = HttpParams::getRequiredParam($this->f3, 'username');
        $name = HttpParams::getRequiredParam($this->f3, 'name');
        $password = HttpParams::getRequiredParam($this->f3, 'password');


        $user = new UserDAO($this->db);
        if($user->hasUserWithUsername($username)) {
            HttpResponse::error(self::USER_ALREADY_EXISTS_ERROR, 406);
        }
        else {
            if($user->add($username, $name, $password))
                HttpResponse::success(self::USER_REGISTERED_WITH_SUCCESS);

            else HttpResponse::error(self::USER_REGISTRATION_PROCESS_ENDS_WITH_ERROR, 500);
        }
    }

    function register_as_facebook() {
        $fb_access_token = HttpParams::getRequiredParam($this->f3, 'access_token');
        $body_array = AuthUtils::getFacebookUserDataByToken($this->f3, $fb_access_token);
        if($body_array) {
            $facebook_userid = $body_array['id'];
            $userDAO = new UserDAO($this->db);
            if($userDAO->existsFacebookUser($facebook_userid)) {
                HttpResponse::error(self::USER_ALREADY_EXISTS_ERROR, 400);
            }
            else {
                $userid = $userDAO->addFacebookUser($body_array['email'], $body_array['name'], $body_array['picture'], $facebook_userid);
                if($userid > 0) {
                    $token = AuthUtils::getGeneratedJWTToken($userid);
                    HttpResponse::success(array('message' => self::USER_REGISTERED_WITH_SUCCESS, 'token' => $token, 'user' => array('name' => $body_array['name'])));
                }
                else {
                    HttpResponse::error(self::USER_REGISTRATION_PROCESS_ENDS_WITH_ERROR, 500);
                }
            }
        }
        else {
            HttpResponse::error(self::GENERAL_FACEBOOK_REGISTRATION_ERROR, 500);
        }
    }

    function login_index() {
        $view = \View::instance();
        echo $view->render('login.html');
    }

    function login() {
        $username = HttpParams::getRequiredParam($this->f3, 'username');
        $password = HttpParams::getRequiredParam($this->f3, 'password');

        $user = new UserDAO($this->db);
        $user_result = $user->getUserByUsername($username);
        if($user_result == null) {
            HttpResponse::error(self::USER_DONT_EXIST, 406);
        }
        else {
            if(!UserDAO::checkPasswords($password, $user_result->password)) {
                HttpResponse::error(self::USER_DONT_EXIST, 406);
            }
            else {
                $token = AuthUtils::getGeneratedJWTToken($user_result->userid);
                HttpResponse::success(array('message' => self::LOGIN_SUCCEED, 'token' => $token, 'user' => array('name' => $user_result->name)));
            }
        }
    }

    function login_as_facebook() {
        $fb_access_token = HttpParams::getRequiredParam($this->f3, 'access_token');

        $body_array = AuthUtils::getFacebookUserDataByToken($this->f3, $fb_access_token);
        if($body_array) {
            if(!key_exists('id', $body_array) || !key_exists('name', $body_array)) {
                HttpResponse::error(self::INVALID_FACEBOOK_REQUEST, 400);
            }
            else {
                $facebook_user_id = $body_array['id'];
                $userDAO = new UserDAO($this->db);
                $user = $userDAO->getFacebookUser($facebook_user_id);
                if($user == null) {
                    HttpResponse::error(self::USER_DONT_EXIST, 400);
                }
                else {
                    $token = AuthUtils::getGeneratedJWTToken($user->userid);
                    HttpResponse::success(array('message' => self::LOGIN_SUCCEED, 'token' => $token, 'user' => array('name' => $body_array['name'])));
                }
            }
        }
        else {
            HttpResponse::error(self::GENERAL_FACEBOOK_LOGIN_ERROR, 500);
        }


    }

    function changepassword_index() {
        $view = \View::instance();
        echo $view->render('change_password.html');
    }

    function changepassword() {
        $oldPassword = HttpParams::getRequiredParam($this->f3, 'oldpassword');
        $newPassword = HttpParams::getRequiredParam($this->f3, 'newpassword');

        $userid = AuthUtils::getVerifiedUserIdOfToken($this->f3); //Token is already verified in this function
        $userDAO = new UserDAO($this->db);
        $userData = $userDAO->getUserById($userid);
        if($userData == null) {
            HttpResponse::error(self::USER_DONT_EXIST, 400);
        }
        else {
            if(!$userData->checkPasswords($oldPassword, $userData->password)) {
                HttpResponse::error(self::BAD_USER_PASSWORD, 403);
            }
            else {
                if(!$userData->checkPasswords($newPassword, $userData->password)) //Check if it's the same password or not
                    $userData->changePassword($newPassword);
                HttpResponse::success(self::USER_PASSWORD_CHANGED_WITH_SUCCESSS);
            }
        }
    }

    function myaccount_index() {
        $view = \View::instance();
        echo $view->render('myaccount.html');
    }

    function myaccount_info() {
        $userid = AuthUtils::getVerifiedUserIdOfToken($this->f3); //Token is already verified in this function
        $userDAO = new UserDAO($this->db);
        $user = $userDAO->getUserById($userid);
        if($user == null) {
            HttpResponse::error(self::USER_DONT_EXIST, 400);
        }
        else {
            HttpResponse::success(array('user' => UserDAO::convertToArray($user)));
        }
    }

    function update_profile() {
        $name = HttpParams::getRequiredParam($this->f3, 'name');
        $userid = AuthUtils::getVerifiedUserIdOfToken($this->f3); //Token is already verified in this function
        $userDAO = new UserDAO($this->db);
        $user = $userDAO->getUserById($userid);
        if($user == null) {
            HttpResponse::error(self::USER_DONT_EXIST, 400);
        }
        else {
            $new_img_url = null;
            $user->name = $name;
            $filepath = FileUploadManager::receiveImage();
            if($filepath) {
                $s3 = new S3Manager($this->f3);
                $new_img_url = $s3->uploadProfilePicture($filepath, $userid);
                $user->img_photo = $new_img_url;
                unlink($filepath);
            }
            $user->update();
            HttpResponse::success(array('message' => self::PROFILE_UPDATED_WITH_SUCCESS, 'photo' => $new_img_url));
        }
    }

    function profile_index() {
        $view = \View::instance();
        echo $view->render('profile.html');
    }

    function search_user() {
        $userDAO = new UserDAO($this->db);
        $pagination = new Pagination($this->f3, $userDAO, null, null, self::LIMIT_OF_USERS_SEARCH_RESULT);
        $array_results = $pagination->retrieveSearchResults();

        HttpResponse::success(array('message' => self::SEARCH_RESULTS_WITH_SUCCESS, 'search' => $array_results));
    }

    function users_index() {
        $view = \View::instance();
        echo $view->render('users.html');
    }
}