<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/23/17
 * Time: 9:33 AM
 */
class FriendController extends MainController
{

    const LIMIT_OF_FRIENDS_RETRIEVING = 20;

    const ERROR_TRYING_TO_MANIPULATE_FRIEND_RELATION_BETWEEN_SAME_USER = "Trying to manipulate a Friend relationship with the same user",
          ALREADY_FRIENDS_ERROR = "Users are already friends",
          NOT_FRIENDS_ERROR = "Both users are not Friends";

    const ADDED_FRIEND_WITH_SUCCESS = "Friend added with success",
          REMOVED_WITH_SUCCESS = "Users are not friends anymore",
          UPDATED_REDIS_AND_MONGO_FRIENDS_WITH_SUCCESS = "Updated Redis and MongoDB Friends with success";

    function getFriends() {
        //Base $f3, UserDAO $userDAO, DB\SQL\Mapper $requestDAO, $limit
        $pagination = new Pagination($this->f3, new UserDAO($this->db), new FriendDAO($this->db), null,self::LIMIT_OF_FRIENDS_RETRIEVING);
        $data = $pagination->retrieve();
        HttpResponse::success($data);
    }

    function removeFriend() {
        $data = $this->getVerifiedRequestData();
        $user = $data['user'];
        $user_result = $data['user_result'];

        $friendDAO = new FriendDAO($this->db);
        if(!$friendDAO->areFriends($user->userid, $user_result->userid)) {
            HttpResponse::error(self::NOT_FRIENDS_ERROR);
        }
        else {
            $userid1 = $user->userid;
            $userid2 = $user_result->userid;
            $friendDAO->removeFriend($userid1, $userid2);
            NodeManager::notifyRemovedFriend($this->f3, $userid1, $userid2);
            HttpResponse::success(self::REMOVED_WITH_SUCCESS);
        }
    }

    function addFriend() {
        $data = $this->getVerifiedRequestData();
        $user = $data['user'];
        $user_result = $data['user_result'];

        $friendshipDAO = new FriendshipRequestDAO($this->db);
        if(!$friendshipDAO->existsFriendship($user->userid, $user_result->userid)) {
            HttpResponse::error(FriendshipRequestController::FRIENDSHIP_REQUEST_DOESNT_EXISTS_ERROR);
        }
        else {
            $friendDAO = new FriendDAO($this->db);
            if($friendDAO->areFriends($user->userid, $user_result->userid)) {
                HttpResponse::error(self::ALREADY_FRIENDS_ERROR);
            }
            else {
                $userid1 = $user->userid;
                $userid2 = $user_result->userid;
                $friendshipDAO->removeFriendship($userid1, $userid2);
                $friendDAO->addFriend($userid1, $userid2);
                NodeManager::notifyAddedFriend($this->f3, $userid1, $userid2);
                HttpResponse::success(self::ADDED_FRIEND_WITH_SUCCESS);
            }
        }
    }

    function updateRedisAndMongo() {
        //Get Unsigned Integer
        $username = HttpParams::getRequiredParam($this->f3, 'username');
        if($username != $this->f3->get('admin_username')) {
            HttpResponse::error(UserController::USER_DONT_EXIST, 400);
        }
        else {
            $password = HttpParams::getRequiredParam($this->f3, 'password');
            if($password != $this->f3->get('admin_password'))
                HttpResponse::error(UserController::USER_DONT_EXIST, 400);
            else {
                $friendDAO = new FriendDAO($this->db);
                $friends = $friendDAO->allFriendsOrdered();
                $users_data = $this->getPreparedFriends($friends);
                NodeManager::updateAllFriends($this->f3, $users_data);
                HttpResponse::success(self::UPDATED_REDIS_AND_MONGO_FRIENDS_WITH_SUCCESS);
            }
        }
    }

    private function getPreparedFriends($friends) {
        $json_friends = array();
        if($friends && count($friends) > 0) {
            $userid = $friends[0]->userid1;
            $user_friends = array();
            foreach ($friends as $f) {
                if($f->userid1 != $userid) {
                    //Append additional data to array
                    array_push($json_friends, array('userid' => $userid, 'friends' => $user_friends));
                    //Init next user vars
                    $userid = $f->userid1;
                    $user_friends = array();
                }

                array_push($user_friends, $f->userid2); //add user friend
            }

            array_push($json_friends, array('userid' => $userid, 'friends' => $user_friends)); //Append last userdata

            $userid = $friends[0]->userid2;
            $user_friends = array();
            foreach($friends as $f) {
                if($f->userid1 != $userid) {
                    //Append additional data to array
                    array_push($json_friends, array('userid' => $userid, 'friends' => $user_friends));
                    //Init next user vars
                    $userid = $f->userid2;
                    $user_friends = array();
                }

                array_push($user_friends, $f->userid1); //add user friend
            }

            array_push($json_friends, array('userid' => $userid, 'friends' => $user_friends)); //Append last userdata
        }
        return array('users' => $json_friends);
    }

    private function getVerifiedRequestData()
    {
        //Get Unsigned Integer
        $username = HttpParams::getRequiredParam($this->f3, 'username');

        //Verify user existence
        $userid = AuthUtils::getVerifiedUserIdOfToken($this->f3); //Token is already verified in this function

        $userDAO = new UserDAO($this->db);
        $user = $userDAO->getUserById($userid);
        if($user == null) {
            HttpResponse::error(UserController::USER_DONT_EXIST, 406);
        }
        else {
            $user_result = $userDAO->getUserByUsername($username);
            if($user_result == null) {
                HttpResponse::error(UserController::USER_DONT_EXIST, 400);
            }
            else {
                if($user->userid == $user_result->userid) {
                    HttpResponse::error(self::ERROR_TRYING_TO_MANIPULATE_FRIEND_RELATION_BETWEEN_SAME_USER);
                }
                else return array('user' => $user, 'user_result' => $user_result);
            }
        }
    }


}