<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/11/17
 * Time: 12:10 AM
 */
class MessageController extends MainController
{
    public function index_board() {
        $view = \View::instance();
        echo $view->render('messageboard.html');
    }

    public function index_message() {
        $view = \View::instance();
        echo $view->render('messages.html');
    }
}