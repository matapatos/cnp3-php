# Cloud-based social network #

This project consists on a cloud-based social network where a user can:

- Create its own profile (e.g. profile picture, name, bio).
- Send and accept friend requests.
- View its friends profile.
- Chat with its friends.

## Architecture ##

This system applies the Software as a Service (SaaS) cloud nomenclature with four machines (see figure bellow).

![System architecture](CloudBasedSystem.png)