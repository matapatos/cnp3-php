<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/23/17
 * Time: 9:34 AM
 */
class FriendDAO extends DB\SQL\Mapper  implements FriendPagination
{
    public function __construct($db)
    {
        parent::__construct($db, 'friend', NULL, 60);
    }

    public function allOfUser($userid, $options = null) {
        $this->load(array('userid1=? or userid2=?', $userid, $userid), $options);
        return $this->query;
    }

    public function countAllOfUser($userid) {
        return $this->count(array('userid1=? or userid2=?', $userid, $userid));
    }

    public function areFriends($userid1, $userid2) {
        $this->load(array('(userid1=? AND userid2=?) OR (userid1=? AND userid2=?)', $userid1, $userid2, $userid2, $userid1));
        return !$this->dry();
    }

    public function addFriend($userid1, $userid2) {
        try {
            $this->copyfrom(array('userid1' => $userid1, 'userid2' => $userid2));
            $this->insert();
            return $this->_id;
        }
        catch(\PDOException $e) {
            return false;
        }
    }

    public function removeFriend($userid1, $userid2) {
        try {
            return $this->erase(array('(userid1=? AND userid2=?) OR (userid1=? AND userid2=?)', $userid1, $userid2, $userid2, $userid1));
        }
        catch(\PDOException $e) {
            return false;
        }
    }

    public function allFriendsOrdered() {
        $this->load(null, array('order' => 'userid1'));
        return $this->query;
    }

    public function getFriends($userid, $userIDs) {
        if(is_array($userIDs)) {
            $userIDs = General::userIDsArrayToString($userIDs);
        }

        $this->load(array('(userid1=? AND userid2 IN ' . $userIDs . ') OR (userid1 IN ' . $userIDs . ' AND userid2=?)', $userid, $userid));
        return $this->query;
    }
}