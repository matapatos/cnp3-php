<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/9/17
 * Time: 5:10 PM
 */
class UserDAO extends DB\SQL\Mapper
{
    public function __construct($db)
    {
        parent::__construct($db, 'user', NULL, 60);
    }

    public function all() {
        $this->all();
        return $this->query;
    }

    public function add($username, $name, $password) {
        $userdata = array('username' => $username, 'name' => $name, 'password' => self::cryptPassword($password));
        return $this->addUser($userdata);
    }

    public function addFacebookUser($username, $name, $photo, $facebook_id) {
        $userdata = array('username' => $username, 'name' => $name, 'userid_facebook' => $facebook_id, 'img_photo' => $photo);
        return $this->addUser($userdata);
    }

    public function existsFacebookUser($facebook_user_id) {
        $user = $this->getFacebookUser($facebook_user_id);
        return ($user != null);
    }

    public function getFacebookUser($facebook_user_id) {
        $this->load(array('userid_facebook=?', $facebook_user_id));
        if(count($this->query) > 0)
            return $this->query[0];
        return null;
    }

    private function addUser($userdata) {
        try {
            $this->copyfrom($userdata);
            $this->insert();
            return $this->_id;
        }
        catch(\PDOException $e) {
            return false;
        }
    }

    public function changePassword($password) {
        $userData = array('password' => self::cryptPassword($password));
        $this->copyfrom($userData);
        $this->update();
    }

    public function hasUserWithUsername($username) {
        self::getUserByUsername($username);
        return !$this->dry();
    }

    public function getUserById($userid) {
        $this->load(array('userid=?', $userid));
        if(count($this->query) > 0)
            return $this->query[0];
        return null;
    }

    public function getUserByUsername($username) {
        $this->load(array('username=?', $username));
        if(count($this->query) > 0)
            return $this->query[0];
        return null;
    }

    public function getUsers($userIDs, $options = null) {
        if(is_array($userIDs)) {
            $userIDs = General::userIDsArrayToString($userIDs);
        }
        $this->load(array('userid IN ' . $userIDs), $options);
        return $this->query;
    }

    public function searchUserBy($query, $userid, $options = null) {
        return $this->db->exec('SELECT DISTINCT u.userid, u.username, u.name, u.img_photo,  IF(f.userid1=' . $userid . ' OR f.userid2=' . $userid . ', true, false) as is_friend, IF(fr.userid2=' . $userid . ', 1, IF(fr.userid1=' . $userid . ', 2, 0)) as has_friend_request FROM user as u LEFT JOIN friend as f ON u.userid=f.userid1 OR u.userid=f.userid2
LEFT JOIN friend_request as fr ON u.userid=fr.userid1 OR u.userid=fr.userid2 WHERE u.name LIKE \'%' . $query . '%\' OR u.username LIKE \'%' . $query . '%\'', $options);
    }

    public function countSearchUserBy($query) {
        return $this->count(array('username LIKE ? OR name LIKE ?', "%" . $query . "%", "%" . $query . "%"));
    }

    public static function cryptPassword($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public static function checkPasswords($password_str, $password_hashed) {
        return password_verify($password_str, $password_hashed);
    }

    public static function convertUsersToArray($users) {
        $users_array = array();
        foreach($users as $u) {
            $u_array = self::convertToArray($u);
            array_push($users_array, $u_array);
        }
        return $users_array;
    }

    public static function convertToArray($user) {
        return array(
            'userid' => $user->userid,
            'username' => $user->username,
            'name' => $user->name,
            'img_photo' => $user->img_photo
        );
    }
}