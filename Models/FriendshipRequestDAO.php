<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/23/17
 * Time: 12:47 PM
 */
class FriendshipRequestDAO extends DB\SQL\Mapper implements FriendPagination
{
    public function __construct($db)
    {
        parent::__construct($db, 'friend_request', NULL, 60);
    }

    // Previous: array('userid1=? or userid2=?', $userid, $userid)
    // It only returns the requests made to him, not the ones that he sent to the others
    public function allOfUser($userid, $options = null) {
        $this->load(array('userid2=?', $userid), $options);
        return $this->query;
    }

    public function countAllOfUser($userid) {
        return $this->count(array('userid1=? or userid2=?', $userid, $userid));
    }

    public function existsFriendship($userid1, $userid2) {
        $this->load(array('(userid1=? AND userid2=?) OR (userid1=? AND userid2=?)', $userid1, $userid2, $userid2, $userid1));
        return !$this->dry();
    }

    public function addFriendship($userid1, $userid2) {
        try {
            $this->copyfrom(array('userid1' => $userid1, 'userid2' => $userid2));
            $this->insert();
            return $this->_id;
        }
        catch(\PDOException $e) {
            return false;
        }
    }

    public function removeFriendship($userid1, $userid2) {
        try {
            return $this->erase(array('(userid1=? AND userid2=?) OR (userid1=? AND userid2=?)', $userid1, $userid2, $userid2, $userid1));
        }
        catch(\PDOException $e) {
            return false;
        }
    }

    public function getFriendships($userid, $userIDs) {
        if(is_array($userIDs)) {
            $userIDs = General::userIDsArrayToString($userIDs);
        }

        $this->load(array('(userid1=? AND userid2 IN ' . $userIDs . ') OR (userid1 IN ' . $userIDs . ' AND userid2=?)', $userid, $userid));
        return $this->query;
    }
}