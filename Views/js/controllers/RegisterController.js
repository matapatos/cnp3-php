/**
 * Created by andre on 5/8/17.
 */

app
    .controller('RegisterController', ['$scope', 'UserService', 'HttpService', function($scope, userService, HttpService) {
        $scope.errorMessage = false;
        $scope.successMessage = false;

        if(userService.isAuth())
            window.location.href = "/messageboard";

        $scope.submitForm = function() {
            $scope.errorMessage = false;
            $scope.successMessage = false;

            var onSuccess = function (data) {
                    var json = JSON.parse(JSON.stringify(data));
                    if(json.data.error) {
                        $scope.errorMessage = json.data.error_code + ": " + json.data.error;
                    }
                    else {
                        $scope.successMessage = json.data.message;
                    }
                },
                params = {
                    username : $scope.username,
                    name : $scope.name,
                    password : $scope.password
                };
            HttpService.post('/register', params, onSuccess);
        }
    }]);