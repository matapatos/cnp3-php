/**
 * Created by andre on 5/8/17.
 */

app
    .controller('UsersController', ['$scope', 'HttpService', 'UserService', 'ErrorHandlerService', function($scope, HttpService, userService, errorHandlerService) {
        $scope.users = [];
        $scope.query = "";
        $scope.currentPage = 1;
        $scope.totalpages = 0;


        $scope.search = function() {
            var successCb = function(data) {
                if(!data|| !data.data) {
                    alert('An error occured while retrieving users');
                    console.error(data);
                }
                else {
                    var json = JSON.parse(data.data);
                    if(!json) {
                        alert('An error occured while retrieving users');
                        console.error(data);
                    }
                    else {
                        if(errorHandlerService.isSuccess(json)) {
                            $scope.users = json.search.results;
                            if("numPages" in json.search)
                                $scope.totalpages = json.search.numPages;
                            console.log($scope.users);
                        }
                    }
                }
            };
            var params = '?query=' + $scope.query + "&pageid=" + $scope.currentPage + '&numPages=1';
            HttpService.get('/search/user' + params, successCb, false, true);
        };

        $scope.isActive = function(x) {
            if(x == $scope.currentPage)
                return 'active';
            return false;
        };

        $scope.getPagination = function() {
            var array = [];
            for(var i = 0; i < $scope.totalpages; i++) {
                array.push(i+1);
            }
            return array;
        };

        $scope.page = function(x) {
            $scope.currentPage = x;
            $scope.search();
        };

        $scope.showProfile = function(user) {
            userService.saveUserProfile(user);
            window.location.href = "/profile";
        };

        $scope.sendFriendshipRequest = function(user) {
            user.has_friend_request = "2";
            HttpService.get('/friendships/add?username=' + user.username, false, false, true);
        };

    }]);