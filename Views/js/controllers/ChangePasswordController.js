/**
 * Created by andre on 5/8/17.
 */

app
    .controller('ChangePasswordController', ['$scope', 'HttpService', 'TokenService', function($scope, HttpService, tokenService) {
        $scope.errorMessage = false;
        $scope.successMessage = false;

        if(!tokenService.getToken())
            window.location.href = "/";

        $scope.submitForm = function() {
            $scope.errorMessage = false;
            $scope.successMessage = false;

            var onSuccess = function (data) {
                    var json = JSON.parse(data.data);
                    if(json.error) {
                        $scope.errorMessage = json.error_code + ": " + json.error;
                    }
                    else {
                        if(json.token)
                            tokenService.saveToken(json.token);

                        $scope.successMessage = json.message;
                    }
                },
                params = {
                    oldpassword : $scope.oldpassword,
                    newpassword : $scope.newpassword
                };
            HttpService.post('/changepassword', params, onSuccess, null, true);
        }
    }]);