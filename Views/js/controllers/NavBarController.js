/**
 * Created by andre on 5/8/17.
 */

app
    .controller('NavBarController', ['$scope', 'LogoutService', 'UserService', function($scope, logoutService, userService) {

        $scope.user = userService.getUser();

        $scope.logout = function () {
            logoutService.logout();
        };

        $scope.showOwnProfile = function() {
            userService.clearUserProfile();
            window.location.href = "/profile";
        };
    }]);