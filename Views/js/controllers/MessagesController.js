/**
 * Created by andre on 6/6/17.
 */
/**
 * Created by andre on 5/8/17.
 */


app
    .controller('MessagesController', ['$scope', 'HttpService', 'UserService', 'ErrorHandlerService', function($scope, HttpService, userService, errorHandlerService) {

        if(!userService.isAuth())
            window.location.href = "/";

        $scope.totalpages = 0;
        $scope.page = 1;
        $scope.messages = [];

        $scope.init = function() {
            $scope.getPage(1); //Initialize messages view
        };

        $scope.getPage = function(page) {
            $scope.page = page;

            var cb = function (data) {
                console.log(data);
                if(!data || !data.data)
                    alert('An error occured retrieving the messages');
                else {
                    if(errorHandlerService.isSuccess(data.data)) {
                        $scope.messages = data.data.data.docs;
                        $scope.totalpages = data.data.data.pages;
                    }
                }
            };
            HttpService.get('http://localhost:8080/messages?page=' + $scope.page, cb);
        };

        $scope.getTimePassed = function(datetime_str) {
            var creation_date = new Date(datetime_str),
                now = new Date();

            var diff = Math.abs(now - creation_date);
            var minutes = (diff/1000) / 60;
            if(minutes <= 59) {
                return parseInt(minutes) + "m ago";
            }
            else {
                var hours = minutes/60;
                if(hours <= 23) {
                    return parseInt(hours) + "h ago";
                }
                else {
                    var days = hours / 24;
                    return parseInt(days) + "d ago";
                }
            }
        };

        $scope.isActive = function(x) {
            if(x == $scope.page)
                return 'active';
            return false;
        };

        $scope.getPagination = function() {
            var array = [];
            for(var i = 0; i < $scope.totalpages; i++) {
                array.push(i+1);
            }
            return array;
        };

        $scope.init();
    }]);