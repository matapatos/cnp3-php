/**
 * Created by andre on 5/8/17.
 */


app
    .controller('MessageBoardController', ['$scope', 'HttpService', 'UserService', 'ErrorHandlerService', 'TokenService', function($scope, HttpService, userService, errorHandlerService, tokenService) {

        if(!userService.isAuth())
            window.location.href = "/";

        $scope.chatmessage = "";
        $scope.totalpages = 0;
        $scope.page = 1;
        $scope.messages = [];
        $scope.socket = null;

        $scope.init = function() {
            $scope.getPage(1); //Initialize messages view

            // You should add auth_token to query while connecting
            // Replace THE_JWT_TOKEN with the valid one
            var token = tokenService.getToken().replace('Bearer ', '');
            $scope.socket = io('http://localhost:9000', {query: 'auth_token=' + token});
            // Authentication failed
            $scope.socket.on('error', function(err) {
                throw new Error(err);
            });
            // Authentication passed
            $scope.socket.on('success', function(data) {
                console.log(data);
            });

            $scope.socket.on('chat message', function(msg){
                $scope.messages.unshift(msg);
                $scope.$apply();
            });
        };

        $scope.sendMessage = function() {
            if($scope.socket)
                $scope.socket.emit('chat message', $scope.chatmessage);
        };

        $scope.getPage = function(page) {
              $scope.page = page;

              var cb = function (data) {
                  console.log(data);
                  if(!data || !data.data)
                      alert('An error occured retrieving the messages');
                  else {
                       if(errorHandlerService.isSuccess(data.data)) {
                            $scope.messages = data.data.data.docs;
                            $scope.totalpages = data.data.data.pages;
                       }
                  }
              };
              HttpService.get('http://localhost:8080/messages?page=' + $scope.page, cb);
        };

        $scope.getTimePassed = function(datetime_str) {
            var creation_date = new Date(datetime_str),
                now = new Date();

            var diff = Math.abs(now - creation_date);
            var minutes = (diff/1000) / 60;
            if(minutes <= 59) {
                return parseInt(minutes) + "m ago";
            }
            else {
                var hours = minutes/60;
                if(hours <= 23) {
                    return parseInt(hours) + "h ago";
                }
                else {
                    var days = hours / 24;
                    return parseInt(days) + "d ago";
                }
            }
        };

        $scope.init();
    }]);