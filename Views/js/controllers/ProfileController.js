/**
 * Created by andre on 5/8/17.
 */

app
    .controller('ProfileController', ['$scope', 'UserService', 'ErrorHandlerService', 'HttpService', function($scope, userService, errorHandlerService, HttpService) {

        if(!userService.isAuth())
            window.location.href = "/login";

        var successCb = function (data) {
            if(!data || !data.data) {
                alert("An error occoured when trying to retrieve the own profile");
            }
            else {
                if(errorHandlerService.isSuccess(data.data)) {
                    var json_data = JSON.parse(data.data);
                    $scope.userProfile = json_data.user;
                    userService.saveUser($scope.user);
                }
            }
        };
        $scope.userProfile = userService.getUserProfile(successCb);


        $scope.totalpages = 0;
        $scope.page = 1;
        $scope.messages = [];
        $scope.errorMessage = false;

        $scope.init = function() {
            $scope.getPage(1); //Initialize messages view
        };

        $scope.getPage = function(page) {
            $scope.errorMessage = false;
            $scope.page = page;

            var cb = function (data) {
                if(!data || !data.data)
                    alert('An error occured retrieving the messages');
                else {
                    if(data.data.error) {
                        $scope.errorMessage = "The user is not a friend of this person, so it can't see his/her messages";
                    }
                    else {
                        $scope.messages = data.data.data.docs;
                        $scope.totalpages = data.data.data.pages;
                    }
                }
            };
            console.log($scope.userProfile);
            HttpService.get('http://localhost:8080/messages/user?page=' + $scope.page + "&userid=" + $scope.userProfile.userid, cb);
        };

        $scope.getTimePassed = function(datetime_str) {
            var creation_date = new Date(datetime_str),
                now = new Date();

            var diff = Math.abs(now - creation_date);
            var minutes = (diff/1000) / 60;
            if(minutes <= 59) {
                return parseInt(minutes) + "m ago";
            }
            else {
                var hours = minutes/60;
                if(hours <= 23) {
                    return parseInt(hours) + "h ago";
                }
                else {
                    var days = hours / 24;
                    return parseInt(days) + "d ago";
                }
            }
        };

        $scope.isActive = function(x) {
            if(x == $scope.page)
                return 'active';
            return false;
        };

        $scope.getPagination = function() {
            var array = [];
            for(var i = 0; i < $scope.totalpages; i++) {
                array.push(i+1);
            }
            return array;
        };

        $scope.init();

    }]);