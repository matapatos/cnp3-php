/**
 * Created by andre on 5/8/17.
 */

app
    .controller('LoginController', ['$scope', 'HttpService', 'TokenService', 'UserService',  function($scope, HttpService, tokenService, userService) {
        $scope.errorMessage = false;

        if(tokenService.getToken())
            window.location.href = "/messageboard";

        $scope.submitForm = function() {
            $scope.errorMessage = false;
            $scope.successMessage = false;

            var onSuccess = function (data) {
                    var json = JSON.parse(JSON.stringify(data));
                    if(json.data.error) {
                        $scope.errorMessage = json.data.error_code + ": " + json.data.error;
                    }
                    else if(!json.data.token) {
                        $scope.errorMessage = 'A problem occoured when trying to login.';
                    }
                    else {
                        var token = json.data.token;
                        tokenService.saveToken(token);
                        userService.saveUser(json.data.user);
                        window.location = "/messageboard";
                    }
                },
                params = {
                    username : $scope.username,
                    password : $scope.password
                };
            HttpService.post('/login', params, onSuccess);
        }
    }]);