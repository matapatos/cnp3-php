/**
 * Created by andre on 5/8/17.
 */

app
    .controller('MyAccountController', ['$scope', 'HttpService', 'TokenService', 'UserService', 'ErrorHandlerService', function($scope, httpService, tokenService, userService, errorHandlerService) {

        $scope.updateProfileMessage = null;

        if(!tokenService.getToken())
            window.location.href = "/";

        var successCb = function(data) {
            if(!data || !data.data) {
                alert("An error occoured when trying to retrieve the own profile");
            }
            else {
                if(errorHandlerService.isSuccess(data.data)) {
                    var json_data = JSON.parse(data.data);
                    $scope.user = json_data.user;
                    userService.saveUser($scope.user);
                }
            }
        };
        $scope.user = userService.getFullAccountInfo(successCb);
        var successCbFriends = function (data) {
            if(data && data.data) {
                var json = JSON.parse(data.data);
                if(json) {
                    if(errorHandlerService.isSuccess(json)) {
                        $scope.friends = json;
                        userService.saveFriends(json);
                    }
                }
            }
            else {
                alert("An error occoured when trying to retrieve all Friends");
                console.error(data);
            }
        };
        $scope.friends = userService.getAllFriends(1, true, successCbFriends);

        var successCbFriendships = function (data) {
            if(data && data.data) {
                var json = JSON.parse(data.data);
                if(json) {
                    if(errorHandlerService.isSuccess(json)) {
                        $scope.friendships = json;
                        userService.saveFriendships(json);
                    }
                }
            }
            else {
                alert("An error occoured when trying to retrieve all Friends");
                console.error(data);
            }
        };
        $scope.friendships = userService.getAllFriendships(1, true, successCbFriendships);


        $scope.updateProfile = function() {
            $scope.updateProfileMessage = null;
            //Create FormData
            var formData = new FormData();
            formData.append("name", $scope.user.name);

            var files = $('#img_photo')[0].files;
            if(files.length > 0) {
                formData.append("img_photo", files[0]);
            }
            //Create success funtion
            var successCb = function(data) {
                if(!data) {
                    alert("An error occoured when trying update profile");
                }
                else {
                    data = JSON.parse(data);
                    $scope.updateProfileMessage = data;
                    console.log(data);
                    if(data.photo != null) {
                        $scope.user.img_photo = data.photo + "?" + new Date();
                    }
                    userService.saveUser($scope.user);
                    $scope.$apply();
                }
            };

            httpService.uploadFile('/profile', formData, successCb);
        };

        $scope.removeFriend = function(user) {
            userService.deleteFriend(user.username);

            $scope.friends.results.remove(user);
            checkFriends();
        };

        $scope.getFriendsPagination = function() {
            return new Array($scope.friends.numPages);
        };

        $scope.getFriendshipsPagination = function() {
            return new Array($scope.friendships.numPages);
        };

        $scope.getFriendsPage = function(pageid) {
            userService.getAllFriends(pageid, false, successCbFriends);
        };

        $scope.getFriendshipsPage = function(pageid) {
            userService.getAllFriendships(pageid, false, successCbFriendships);
        };

        $scope.acceptFriendship = function(user, index) {
            httpService.get('/friends/add?username=' + user.username, false, false, true);

            $scope.friends.results.splice(0, 0, user);
            $scope.friendships.results.splice(index, 1);
            checkFriendships();
        };

        $scope.removeFriendship = function(user, index) {
            httpService.get('/friendships/remove?username=' + user.username, false, false, true);

            $scope.friendships.results.splice(index, 1);
            checkFriendships();
        };

        $scope.removeFriend = function(user, index) {
            httpService.get('/friends/remove?username=' + user.username, false, false, true);

            $scope.friends.results.splice(index, 1);
            checkFriends();
        };

        $scope.showProfile = function(user) {
            userService.saveUserProfile(user);
            window.location.href = "/profile";
        };

        function checkFriendships() {
            if($scope.friendships.results.length === 0)
                $scope.friendships.numPages = 0;
        }

        function checkFriends() {
            if($scope.friends.results.length === 0)
                $scope.friends.numPages = 0;
        }
    }]);