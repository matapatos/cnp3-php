/**
 * Created by andre on 5/8/17.
 */
app
    .service('ErrorHandlerService', ['LogoutService', function(LogoutService) {
        var service = {
            isSuccess : isSuccess
        };
        return service;

        function isSuccess(data, notifyFunc) {
            if(data.error) {
                var EXPIRED_TOKEN_ERROR_CODE = 401;

                if(typeof notifyFunc !== 'function')
                    notifyFunc = alert;

                notifyFunc(data.error_code + ": " + data.error);
                if(data.error_code === EXPIRED_TOKEN_ERROR_CODE) {
                    LogoutService.logout();
                    location.href = "/login";
                }
                return false;
            }
            return true;
        }

    }]);/**
 * Created by andre on 5/8/17.
 */
