/**
 * Created by andre on 5/8/17.
 */
app
    .service('LogoutService', ['UserService', 'FacebookService', function(userService, facebookService) {
        var service = {
            logout : logout
        };
        return service;

        function logout() {
            userService.logout();
            facebookService.logout(function() {
                window.location.href = "/";
            });
        }
    }]);