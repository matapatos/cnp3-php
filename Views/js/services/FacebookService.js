/**
 * Created by andre on 5/15/17.
 */
app
    .service('FacebookService', ['$rootScope', 'HttpService', 'UserService', function($rootScope, HttpService, userService) {

        var service = {
            watchLoginChange : watchLoginChange,
            logout : logout
        };
        return service;

        function watchLoginChange() {
            if(!userService.isAuth())
                getFacebookAccessToken();
        }

        function logout(cb) {
            if(FB && FB.getAccessToken() !== null) {
                FB.logout(function (response) {
                    if(typeof cb === 'function')
                        cb();
                });
            }
            else if(typeof cb === 'function')
                cb();
        }

        function getFacebookAccessToken() {
            FB.Event.subscribe('auth.authResponseChange', function (res) {

                if(!hasSentRequest()) {
                    if (res.status === 'connected' && res.authResponse && res.authResponse.accessToken) {

                        /*
                         The user is already logged,
                         is possible retrieve his personal info
                         */
                        var onSuccess = function (data) {
                                var json = JSON.parse(JSON.stringify(data));
                                if(json.data.error) {
                                    alert(json.data.error_code + ": " + json.data.error);
                                    console.error(data);
                                    enableRequest();
                                }
                                else if(!json.data.token) {
                                    alert('No authentication ID was sent to the client to being able to login');
                                    console.error(data);
                                    enableRequest();
                                }
                                else {
                                    var token = json.data.token;
                                    userService.auth(token);
                                    userService.saveUser(json.data.user);
                                    window.location = "/messageboard";
                                }
                            },
                            params = {
                                access_token : res.authResponse.accessToken
                            };

                        HttpService.post(window.location.pathname + 'asfacebook', params, onSuccess);
                        disableRequest();

                        /*
                         This is also the point where you should create a
                         session for the current user.
                         For this purpose you can use the data inside the
                         res.authResponse object.
                         */
                    }
                    else {


                     //The user is not logged to the app, or into Facebook:
                     //destroy the session on the server.

                        alert('An error occoured when trying to logged in as Facebook user. Try again');
                        console.error(res);
                     }
                }

            });
        }

        function hasSentRequest() {
            return (typeof $rootScope.alreadySent !== 'undefined' && $rootScope.alreadySent);
        }

        function enableRequest() {
            $rootScope.alreadySent = false;
        }

        function disableRequest() {
            $rootScope.alreadySent = true;
        }

    }]);