/**
 * Created by andre on 5/8/17.
 */
app
    .service('HttpService', ['$http', 'TokenService', function($http, tokenService) {
        var service = {
            get : getRequest,
            post : postRequest,
            uploadFile : uploadFile
        };
        return service;

        function getRequest(url, successCb, errorCb, customTransform) {
            httpRequest('GET', url, null, successCb, errorCb, customTransform);
        }

        function postRequest(url, data, successCb, errorCb, customTransform) {
            httpRequest('POST', url, data, successCb, errorCb, customTransform);
        }

        function httpRequest(method, url, data, successCb, errorCb, customTransform) {
            if(typeof successCb !== 'function') {
                successCb = function(resp) {};
                console.warn("Success callback in HttpService is not a function.");
            }
            if(typeof errorCb !== 'function') {
                errorCb = httpError;
            }

            var token = tokenService.getToken();
            if(token)
                $http.defaults.headers.common.Authorization = token;

            if(typeof customTransform !== 'undefined') {
                $http({
                    method: method,
                    url: url,
                    params: data,
                    transformResponse : JSONTransform
                }).then(successCb, errorCb);
            }
            else {
                $http({
                    method: method,
                    url: url,
                    params: data
                }).then(successCb, errorCb);
            }
        }

        function JSONTransform(data, headers, status) {
            var index = data.indexOf('{');
            if(index >= 0) {
                var json_str = data.substring(index, data.length);
                if(json_str) {
                    return json_str;
                }
            }
            return false;
        }

        function httpError(response) {
            alert("An error occour during a request to the server. Try again. If this error persists, please contact the administrator.");
            console.log(response);
        }

        function uploadFile(url, formData, successCb, errorCb) {
            if(typeof successCb !== 'function') {
                successCb = function(resp) {};
                console.warn("Success callback in HttpService is not a function.");
            }
            if(typeof errorCb !== 'function') {
                errorCb = function(data) {
                    if(data && data.responseText) {
                        var str_data = data.responseText;
                        var index = str_data.indexOf('{');
                        if(index >= 0) {
                            var json_str = str_data.substring(index, str_data.length);
                            if(json_str) {
                                successCb(JSON.parse(JSON.stringify(json_str)));
                                return;
                            }
                        }
                    }
                    httpError(data);
                };
            }

            var token = tokenService.getToken();
            $.ajax({
                url: url,
                data: formData,
                headers : {
                    'Authorization' : token
                },
                type: 'POST',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false // NEEDED, DON'T OMIT THIS
                // ... Other options like success and etc
            })
            .done(successCb)
            .fail(errorCb);
        }
    }]);