/**
 * Created by andre on 5/16/17.
 */
/**
 * Created by andre on 5/8/17.
 */
app
    .service('UserService', ['HttpService', 'TokenService', function(httpService, tokenService) {
        var service = {
            saveUser : saveUser,
            clearUser : clearUser,
            getUser : getUser,
            getFullAccountInfo : getFullAccountInfo,
            logout : logout,
            isAuth : isAuth,
            auth : auth,
            getAllFriends : getAllFriends,
            getAllFriendships : getAllFriendships,
            saveFriends : saveFriends,
            saveFriendships : saveFriendships,
            deleteFriend : deleteFriend,
            getUserProfile : getUserProfile,
            saveUserProfile : saveUserProfile,
            clearUserProfile : clearUserProfile
        };
        return service;

        function saveUser(user) {
            if(user === null || typeof user === 'undefined') {
                console.warn('Trying to save empty User');
            }
            else {
                currentUser = getUser();
                if(currentUser !== null && typeof currentUser !== 'undefined') {
                    if(user.hasOwnProperty("username")) {
                        currentUser.username = user.username;
                    }
                    if(user.hasOwnProperty("name"))
                        currentUser.name = user.name;
                    if(user.hasOwnProperty("img_photo"))
                        currentUser.img_photo = user.img_photo;

                    user = currentUser;
                }
                window.localStorage.setItem('user', JSON.stringify(user));
            }
        }

        function clearUser() {
            window.localStorage.removeItem('user');
        }

        function getUser() {
            user = window.localStorage.getItem('user');
            if(user !== null && typeof user !== 'undefined')
                return JSON.parse(user);

            return null;
        }

        function getFullAccountInfo(successCb) {
            user = getUser();
            if(user === null || !user.img_photo) {
                httpService.get('/myaccount/user', successCb, null, true);
            }
            return user;
        }

        function getAllFriends(pageid, numPages, successCb) {
            var url = '/friends?pageid=' + pageid;
            if(numPages)
                url += '&numPages=1';
            httpService.get(url, successCb, null, true);
            friends = getObjecFromLocalStorage('friends');
            return friends;
        }

        function getObjecFromLocalStorage(name) {
            array = window.localStorage.getItem(name);
            if(array !== null && typeof array !== 'undefined')
                return JSON.parse(array);
            return {"numPages" : 0, "results" : []};
        }

        function getAllFriendships(pageid, numPages, successCb) {
            var url = '/friendships?pageid=' + pageid;
            if(numPages)
                url += '&numPages=1';
            httpService.get(url, successCb, null, true);
            friendships = getObjecFromLocalStorage('friendships');
            return friendships;
        }

        function saveFriends(friends) {
            if(friends === null || typeof friends === 'undefined') {
                console.warn('Trying to save empty friends');
            }
            else {
                window.localStorage.setItem('friends', JSON.stringify(friends));
            }
        }

        function saveFriendships(friendships) {
            if(friendships === null || typeof friendships === 'undefined') {
                console.warn('Trying to save empty friends');
            }
            else {
                window.localStorage.setItem('friendships', JSON.stringify(friendships));
            }
        }

        function logout() {
            tokenService.clearToken();
            clearUser();
        }

        function isAuth() {
            return tokenService.hasToken();
        }

        function auth(token) {
            tokenService.saveToken(token);
        }

        function deleteFriend(username, successCb) {
            httpService.get('/friends/remove?username' + username, successCb);
        }

        function getUserProfile(successCb) {
            var profile_user = window.localStorage.getItem('user_profile');
            if(profile_user === null || typeof profile_user === 'undefined')
                return getFullAccountInfo(successCb);
            else return JSON.parse(profile_user);
        }

        function saveUserProfile(user) {
            if(user === null || typeof user === 'undefined') {
                console.warn('Trying to save empty profile user');
            }
            else {
                window.localStorage.setItem('user_profile', JSON.stringify(user));
            }
        }

        function clearUserProfile() {
            window.localStorage.removeItem('user_profile');
        }
    }]);