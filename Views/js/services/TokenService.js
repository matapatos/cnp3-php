/**
 * Created by andre on 5/8/17.
 */
app
    .service('TokenService', [function() {
        var service = {
            saveToken : saveToken,
            getToken : getToken,
            clearToken : clearToken,
            hasToken : hasToken
        };
        return service;

        function saveToken(token) {
            if(!token) {
                console.warn('Trying to save empty token');
            }
            else window.localStorage.setItem('token', token);
        }

        function clearToken() {
            window.localStorage.removeItem('token');
        }

        function getToken() {
            return window.localStorage.getItem('token');
        }

        function hasToken() {
            return window.localStorage.getItem('token') !== null;
        }
    }]);