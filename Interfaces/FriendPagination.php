<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 5/23/17
 * Time: 12:51 PM
 *
 * Description:
 *
 * Necessary for DAO classes that uses the Utils/Pagination class, for User <--> Friend relations, such as FriendRequests and Friends itself
 */
interface FriendPagination
{
    public function allOfUser($userid, $options = null);

    public function countAllOfUser($userid);
}